package test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javafx.geometry.HorizontalDirection;
import javafx.geometry.Side;
import javafx.geometry.VerticalDirection;
import javafx.util.Pair;
import model.entities.Ball;
import model.entities.BallBuilder;

/**
 * Classe di test, necessaria per il controllo dei rimbalzi standard. {@link Ball#doOnCollision(Side)} 
 * Si riesce a testare, inoltre, il metodo {@link Ball#getDirection()}
 */
class TestBallBounce {
    private static final Optional<HorizontalDirection> RIGHT = Optional.of(HorizontalDirection.RIGHT);
    private static final Optional<HorizontalDirection> LEFT = Optional.of(HorizontalDirection.LEFT);
    private static final Optional<VerticalDirection> UP = Optional.of(VerticalDirection.UP);
    private static final Optional<VerticalDirection> DOWN = Optional.of(VerticalDirection.DOWN);

    private static final int ANGLE = 30;
    private static final int RECT_ANGLE = 90;

    private Ball ball, ballOrizz, ballVert;
    @BeforeEach
    public void createData() {
        /* Nota: angoli calcolati dalla linea orizzontale in senso orario. */
        this.ball = new BallBuilder().position(0, 0)
                .angle(ANGLE)
                .build();

        this.ballOrizz = new BallBuilder().position(0, 0)
                .radius(1)
                .angle(0)
                .build();

        this.ballVert = new BallBuilder().position(0, 0)
                .radius(1)
                .angle(RECT_ANGLE)
                .build();
    }

    @Test
    void testDoOnCollision() {
        assertEquals(new Pair<>(RIGHT, DOWN), ball.getDirection());
        ball.doOnCollision(Side.RIGHT);
        assertEquals(new Pair<>(LEFT, DOWN), ball.getDirection());
        ball.doOnCollision(Side.BOTTOM);
        assertEquals(new Pair<>(LEFT, UP), ball.getDirection());
        ball.doOnCollision(Side.LEFT);
        assertEquals(new Pair<>(RIGHT, UP), ball.getDirection());
        ball.doOnCollision(Side.TOP);
        assertEquals(new Pair<>(RIGHT, DOWN), ball.getDirection());
        ball.doOnCollision(Side.BOTTOM);
        assertEquals(new Pair<>(RIGHT, UP), ball.getDirection());
        ball.doOnCollision(Side.RIGHT);
        assertEquals(new Pair<>(LEFT, UP), ball.getDirection());
        ball.doOnCollision(Side.TOP);
        assertEquals(new Pair<>(LEFT, DOWN), ball.getDirection());
        ball.doOnCollision(Side.LEFT);
        assertEquals(new Pair<>(RIGHT, DOWN), ball.getDirection());


        assertEquals(new Pair<>(RIGHT, Optional.empty()), ballOrizz.getDirection());
        ballOrizz.doOnCollision(Side.RIGHT);
        assertEquals(new Pair<>(LEFT, Optional.empty()), ballOrizz.getDirection());
        ballOrizz.doOnCollision(Side.BOTTOM);
        assertEquals(new Pair<>(LEFT, Optional.empty()), ballOrizz.getDirection());


        assertEquals(new Pair<>(Optional.empty(), DOWN), ballVert.getDirection());
        ballVert.doOnCollision(Side.BOTTOM);
        assertEquals(new Pair<>(Optional.empty(), UP), ballVert.getDirection());
        ballVert.doOnCollision(Side.RIGHT);
        assertEquals(new Pair<>(Optional.empty(), UP), ballVert.getDirection());

    }
}
