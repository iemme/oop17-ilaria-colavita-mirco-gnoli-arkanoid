package view.utils;

import javafx.scene.image.Image;

/**
 * Enumerazione per la gestione di tutti le immagini da visualizzare.
 */
public enum ImageViewObject {

    /**
     * 
     */
    SFONDO_MENU("Background/Sfondo_menu.jpg"),
    /**
     * 
     */
    SFONDO_ARENA("Background/Sfondo_arena.jpg"),
    /**
     * 
     */
    LATERAL_WALL("Entity/Wall_Vert.png"),
    /**
     * 
     */
    HORIZONTAL_WALL("Entity/Wall_Oriz.png"),
    /**
     * 
     */
    BACK_ICON("Back_Icon.png"),
    /**
     * 
     */
    BIG_BAR_P("PowerUp/BigBar.png"),
    /**
     * 
     */
    FIRE_BALL_P("PowerUp/FireBall.png"),
    /**
     * 
     */
    LASER_BAR_P("PowerUp/LaserBar.png"),
    /**
     * 
     */
    LITTLE_BAR_P("PowerUp/LittleBar.png"),
    /**
     * 
     */
    MORE_LIFE_P("PowerUp/MoreLife.png"),
    /**
     * 
     */
    MULTIPLE_BALL_P("PowerUp/MultipleBall.png"),
    /**
     * 
     */
    DEFAULT_P("PowerUp/Default.png");

    private Image image;

    /** 
     * @param String nome del file
     */
    ImageViewObject(final String name) {
        this.image = new Image(this.getClass().getClassLoader().getResourceAsStream(name));
    }

    /**
     * @return {@link Image}
     */
    public Image getImage() {
        return this.image;
    }
}
