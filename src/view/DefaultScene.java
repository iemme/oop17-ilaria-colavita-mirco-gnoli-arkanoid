package view;

import java.util.ArrayList;
import java.util.List;

import controller.Controller;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import model.GameMode;
import view.utils.ImageViewObject;
/**
 * Scena del menù principale. Estente {@link Scene}. Implementa {@link SceneMenu}.
 */
public class DefaultScene extends Scene implements SceneMenu {

    private static final int TITLE_HEIGHT = (int) (MainMenu.MAIN_WINDOW_HEIGHT * 0.1); //90
    private static final int VERTICAL_OFFSET = (int) (MainMenu.MAIN_WINDOW_HEIGHT * 0.025); //20

    private final Stage stage;

    /**
     * Costruttore di default.
     * @param stage - Stage in cui la scena verr� rappresentata.
     */
    public DefaultScene(final Stage stage) {
        super(new BorderPane());
        this.stage = stage;
        this.stage.setTitle(getTitle());

        ((BorderPane) this.getRoot()).setTop(topPane());
        ((BorderPane) this.getRoot()).setCenter(centerPane());

        final BackgroundImage bgimg = new BackgroundImage(ImageViewObject.SFONDO_MENU.getImage(),  BackgroundRepeat.NO_REPEAT, BackgroundRepeat.ROUND, BackgroundPosition.CENTER,  new BackgroundSize(BackgroundSize.AUTO, BackgroundSize.AUTO, false, false, true, true));
        ((BorderPane) this.getRoot()).setBackground(new Background(bgimg));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final Node centerPane() {
        final List<Button> buttons = new ArrayList<>();

        final Button classic = new MenuButton("Classic game");
        classic.setOnAction(e -> {
            stage.close();
            new Thread(new Controller(GameMode.CLASSIC)).start();
        });

        final Button standard = new MenuButton("Standard game");
        standard.setOnAction(e -> {
            stage.close();
            new Thread(new Controller(GameMode.STANDARD)).start();
        });

        final Button survival = new MenuButton("Survival game");
        survival.setOnAction(e -> {
            stage.close();
            new Thread(new Controller(GameMode.SURVIVAL)).start();
        });

        final Button score = new MenuButton("Score");
        score.setOnAction(e -> this.stage.setScene(new HighscoreView(stage)));

        final Button credits = new MenuButton("Credits");
        credits.setOnAction(e -> this.stage.setScene(new CreditsView(stage)));

        buttons.add(classic);
        buttons.add(standard);
        buttons.add(survival);
        buttons.add(score);
        buttons.add(credits);

        final GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setVgap(VERTICAL_OFFSET);

        for (int i = 0; i < buttons.size(); i++) {
            grid.add(buttons.get(i), 0, i);
        }
        return grid;
    }

    /**
     * 
     */
    @Override
    public final Node topPane() {
        final FlowPane p = new FlowPane();
        p.setAlignment(Pos.CENTER);

        final Label title = new Label("Arkanoid");
        title.setFont(Font.font("Serif", TITLE_HEIGHT));
        title.setTextFill(Color.RED);

        p.getChildren().add(title);
        return p;
    }

    /**
     * In questa scena il bottomPane non è rappresentato.
     */
    @Override
    public Node bottomPane() {
        return null;
    }

    /**
     * 
     */
    @Override
    public final String getTitle() {
        return "Menù principale";
    }

    /**
     * Classe che rappresenta bottoni personalizzati del men� principale.
     * 
     * @author Gnoli Mirco
     */
    private class MenuButton extends Button {

        private final int menuButtonWidth = (int) (stage.getWidth() * 0.6);
        private final Font buttonFont = new Font("Algerian", (stage.getHeight() * 0.04));

        MenuButton(final String text) {
            super(text);

            this.setAlignment(Pos.CENTER);
            this.setFont(buttonFont);
            this.setPrefWidth(menuButtonWidth);
        }
    }
}
