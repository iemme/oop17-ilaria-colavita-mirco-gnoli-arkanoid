package model;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.Optional;

import model.entities.Ball;
import model.entities.Bar;
import model.entities.Brick;
import model.entities.Entity;
import model.entities.IEntityFactory;
import model.entities.EntityThatMoves;
import model.entities.PowerUp;
import model.entities.Projectile;
import model.entities.Wall;
import utility.CollisionUtility;

/**
 * Classe astratta che modella una partita avanzata. Implementa {@link AdvancedGame}.
 * E' possibile interagire con PowerUp.
 * 
 * @see PowerUp
 */
public abstract class AbstractAdvancedGame extends BasicGame implements AdvancedGame {
    private static final int MAX_BALL_IN_GAME = 30;
    private static final int MAX_POWER_UP_IN_GAME = 10;
    private static final int POWER_UP_SCORE = 100;

    private List<PowerUp> pow;
    private List<Projectile> projectiles;

    /**
     * Costruttore che inizializza tutti i parametri di gioco.
     */
    public AbstractAdvancedGame() {
        this.pow = new ArrayList<>();
        this.projectiles = new ArrayList<>();
    }

    @Override
    public final void incLives() {
        this.setLives(this.getLives() + 1);
    }

    /**
     * {@inheritDoc}
     */
    protected void breakBrick(final Integer row, final Brick brick) {
        final List<Brick> newRow = new ArrayList<>(this.getBricks().get(row));
        super.breakBrick(row, brick);

        newRow.remove(brick);

        if (newRow.isEmpty()) {
            doIfBricksRowIsEmpty(row, brick);
        }

        final Optional<PowerUp> pu = this.getFactory().randomPowerUp(brick);
        if (pu.isPresent()) {
            addEntity(pu.get());
        }
    }


    /**
     * Metodo astratto per gestire il caso in cui l'ultimo mattone di una riga sia stato distrutto.
     *
     * @param row - numero della riga
     * @param template - ultimo {@link Brick} della riga, andato distrutto.
     */
    protected abstract void doIfBricksRowIsEmpty(int row, Brick template);

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateModel() {
        final List<Entity> movesEntities = getAllEntities().stream().filter(ent -> ent instanceof EntityThatMoves).collect(Collectors.toList());
        final List<Entity> staticEntities = getAllEntities().stream().filter(ent -> !(ent instanceof EntityThatMoves)).collect(Collectors.toList());
        movesEntities.forEach(e -> ((EntityThatMoves) e).refreshPosition());

        for (final Entity movesEnt : movesEntities) {
            for (final Entity staticEnt : staticEntities) {
                if (CollisionUtility.intersects(movesEnt, staticEnt)) {
                    if (movesEnt instanceof Ball) {
                        ((Ball) movesEnt).bounce(staticEnt);
                    } else if (movesEnt instanceof PowerUp && staticEnt.getClass().equals(Bar.class)) {
                        ((PowerUp) movesEnt).active(this);
                        this.incScore(POWER_UP_SCORE);
                        this.removeEntity(movesEnt);
                    } else if (movesEnt instanceof Projectile && (staticEnt.getClass().equals(Wall.class) || staticEnt.getClass().equals(Brick.class))) {
                        this.removeEntity(movesEnt);
                    }
                    if (!(movesEnt instanceof PowerUp) && staticEnt.getClass().equals(Brick.class)) {
                        if (((Brick) staticEnt).getCounter() > 1) {
                            ((Brick) staticEnt).decCounter();
                        } else {
                            this.removeEntity(staticEnt);
                        }
                    }
                }
            }

            if (movesEnt instanceof Ball) {
                outOfBound((Ball) movesEnt);
            }
            if (movesEnt.getMaxY() > ModelCostant.WORLD_HEIGHT) {
                removeEntity(movesEnt);
            }
        }

    }

    /**
     * Rimuove un'entit� dal gioco.
     * 
     * @param entity - entit� da rimuovere
     */
    @Override
    public final void removeEntity(final Entity entity) {
        super.removeEntity(entity);

        if (entity instanceof Projectile) {
            this.projectiles.remove((Projectile) entity);
        }

        if (entity instanceof PowerUp) {
            this.pow.remove((PowerUp) entity);
        }
    }

    @Override
    public final void addEntity(final Entity entity) {
        if (entity instanceof Ball && this.getBalls().size() < MAX_BALL_IN_GAME) {
            this.getBalls().add((Ball) entity); 
        }

        if (entity instanceof Projectile) {
            this.projectiles.add((Projectile) entity);
        }

        if (entity instanceof PowerUp && this.pow.size() < MAX_POWER_UP_IN_GAME) {
            this.pow.add((PowerUp) entity);
        }
    }

    @Override
    public final List<Entity> getAllEntities() {
        final List<Entity> list = super.getAllEntities();
        list.addAll(this.pow);
        list.addAll(this.projectiles);
        return list;
    }

    @Override
    public final IEntityFactory getFactory() {
        return super.getFactory();
    }

    /**
     * {@inheritDoc}
     */
    protected final void lostLife() {
        super.lostLife();
        this.pow = new ArrayList<>();
        this.projectiles = new ArrayList<>();
    }
}
