package model;

import model.entities.Entity;
import model.entities.IEntityFactory;

/**
 * Interfaccia che modella una logica di gioco che interagisce con i power-up.
 * Estende {@link IBasicGame}
 * 
 * @see PowerUp
 * @see PowerUpType
 */
public interface AdvancedGame extends IBasicGame {

    /**
     * Metodo per ottenere la classe factory.
     * @return IEntityFactory
     */
    IEntityFactory getFactory();

    /**
     * Aggiunge una vita.
     */
    void incLives();

    /**
     * Aggiunge un'entit� al gioco.
     * @param entity 
     */
    void addEntity(Entity entity);
}
